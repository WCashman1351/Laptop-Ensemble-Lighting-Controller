
#include "arduinoFFT.h"
#include <WS2812B.h>

#define NUM_LEDS 70
#define samples 32
#define BIN_NUM 5

WS2812B strip  = WS2812B(NUM_LEDS);
arduinoFFT FFT = arduinoFFT(); 

double vReal[samples], vImag[samples], Prev_Bins[BIN_NUM];
const uint8_t Bin_Alloc[5] = {1, 2, 4, 8, 16};
double bin_Max, Peak_Value = 0, s = 0.5;

void setup()
{
  strip.begin();
  Serial.begin(115200);
  // Initializes the pins on the port
  pinMode(PA0, INPUT_ANALOG);
  pinMode(PB0, OUTPUT);  digitalWrite(PB0, LOW);
  pinMode(PA6, OUTPUT);  digitalWrite(PA6, HIGH);
}

void loop()
{
  // Sample values
  for (uint8_t i = 0; i < samples; i++) {delayMicroseconds(26); vReal[i] = analogRead(PA0) - 2046; vImag[i] = 0.0;}
  
  // Compute FFT
  FFT.Windowing(vReal, samples, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
  FFT.Compute(vReal, vImag, samples, FFT_FORWARD); /* Compute FFT */
  FFT.ComplexToMagnitude(vReal, vImag, samples); /* Compute magnitudes */

  // Get the max
  Peak_Value *= 0.95; // Decay of the past max height
  for (uint8_t i = 0; i < samples; i++) {Peak_Value = max(Peak_Value, vReal[i]);}

  uint8_t counter = 0;
  double Scaled_Val;
  uint8_t LEDs_Activated, startInd;

  // Map the values into array
  for (uint8_t i = 0; i < BIN_NUM; i++) { 
    bin_Max = vReal[counter]; counter++;
    
    // Get the max value of each of the bins
    for (uint8_t k = 1; k < Bin_Alloc[i]; k++) {bin_Max = max(bin_Max, vReal[counter]); counter++;}
    
    // Scaling and mapping to LEDS
    Scaled_Val = log((bin_Max / Peak_Value) + 1) * 20;
    LEDs_Activated = min(14, int(Prev_Bins[i]*s + Scaled_Val*(1-s))); // The displayed value is a linear interpolation of the current value and the previous value

    // Fill in the LEDs
    if (i % 2 == 0) {   
      for (uint8_t k=0; k < LEDs_Activated; k++) {strip.setPixelColor((14*i)+k, strip.Color(255, 0, 0));}
      for (uint8_t k=LEDs_Activated; k < 14; k++) {strip.setPixelColor((14*i)+k, strip.Color(0, 0, 0));}
    } else {
      for (uint8_t k=0; k < 14-LEDs_Activated; k++) {strip.setPixelColor((14*i)+k, strip.Color(0, 0, 0));}
      for (uint8_t k=14-LEDs_Activated; k < 14; k++) {strip.setPixelColor((14*i)+k, strip.Color(255, 0, 0));}
    }
    Prev_Bins[i] = Scaled_Val;  // For linear interpolation purposes
    strip.show();
  }
}
